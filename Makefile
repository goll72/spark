CFLAGS ?= -O2
CFLAGS += -Iinclude -D_XOPEN_SOURCE=700
PREFIX = /usr/local

build:
	mkdir -p bin
	$(CC) $(CFLAGS) src/spark.c src/fifo.c src/sig_handle.c -o bin/spark
	$(CC) $(CFLAGS) src/svmgr.c src/sig_handle.c src/dir.c -o bin/svmgr
	$(CC) $(CFLAGS) src/ssv.c src/dir.c src/fifo.c src/sig_handle.c -o bin/ssv
	$(CC) $(CFLAGS) src/svctl.c -o bin/svctl
	$(CC) $(CFLAGS) src/schpst.c -o bin/schpst
	$(CC) $(CFLAGS) src/shalt.c -o bin/shalt
	$(CC) $(CFLAGS) src/slog.c src/dir.c src/sig_handle.c -o bin/slog

clean:
	rm -rf bin

install:
	ln -sf shalt bin/spoweroff
	ln -sf shalt bin/sreboot
	mkdir -p $(DESTDIR)$(PREFIX)
	cp -Rf bin $(DESTDIR)$(PREFIX)
