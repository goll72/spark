#ifndef _SERVICE_H_
#define _SERVICE_H_

#include <sys/types.h>
#include <time.h>

/* possible states */
#define SV_DOWN 1
#define SV_UP 2
#define SV_ONCE 3

/* possible files */
#define HAS_FINISH (1 << 0)
#define HAS_PGRP (1 << 1)

struct svdir {
    char files;
    char state;
    pid_t pid;
    time_t time_started; 
};


#endif /* _SERVICE_H_ */
