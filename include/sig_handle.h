#ifndef _SIG_HANDLE_H_
#define _SIG_HANDLE_H_

#include <stddef.h>

void sig_handle(int signal, void (*handler)(int));

#endif
