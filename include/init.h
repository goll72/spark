#ifndef _INIT_H_
#define _INIT_H_

#define STAGE_DIR "/etc/spark/stage"
#define STOP_FIFO "/etc/spark/stop"

enum halt {
    KEXEC = 1,
    REBOOT,
    POWEROFF,
    HALT
};

#endif 
