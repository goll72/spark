#ifndef _FIFO_H_
#define _FIFO_H_

#include <sys/types.h>

int mkfifoat_f(int, char *, mode_t);

int is_fifo(char *);

#endif
