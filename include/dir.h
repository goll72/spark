#ifndef _DIR_H_
#define _DIR_H_

#include <sys/types.h>

/* creates a directory, deleting the
 * oiginal file if there was one and
 * it was not a directory, making sure
 * the directory will have the right
 * permissions
 */
int mkdirat_f(int, char *, mode_t);

int is_dir(char *);

#endif
