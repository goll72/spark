spark
=====

spark is a daemontools and runit-inspired init system for Linux.

Some differences from daemontools / runit:
 * Services are tracked using pathnames, not inodes, which allows for multiple symlinks pointing to the same directory to be used for different services that behave similarly. It does require a change to the hierarchy of the service directory, however.
 * No polling is used for service directory updates, Linux' inotify subsystem is used instead.
 * Integrates with kexec.
