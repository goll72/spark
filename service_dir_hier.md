service hierarchy 
=================

(the items marked with a `*` are spark extensions)


```
sv
|
`- run
`- check
`- finish
`- down
`- once *
`- pgrp *
|
`- log
   |
   ` ...
```

* run - file that will be executed
* check - checks if the service is running, the service is considered to be up if check returns 0
* finish - will be executed after `./run` exits to do clean-up tasks
* down - makes the service stay in the down state by default
* once - does not restart the service automatically
* pgrp - runs the service in a new process group, as a session leader
