#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <limits.h>
#include <fcntl.h>
#include <err.h>
#include <poll.h>
#include <time.h>

#include <sys/stat.h>
#include <sys/wait.h>

#include "sig_handle.h"
#include "dir.h"
#include "service.h"
#include "fifo.h"

#define INT_STRLEN ((CHAR_BIT * sizeof(int) - 1) / 3 + 2)

struct svdir sv[2] = {0};

int logfd, fifofd, statusfd, pipefd[2];
int has_log;

char *dir;

void run_service();
void run_log_service();

void check_files(int fd, int svnum)
{
    if (faccessat(fd, "finish", X_OK, 0) == 0) 
        sv[svnum].files |= HAS_FINISH;
    else
        sv[svnum].files &= ~HAS_FINISH;

    if (faccessat(fd, "pgrp", F_OK, 0) == 0)
        sv[svnum].files |= HAS_PGRP;
    else
        sv[svnum].files &= ~HAS_PGRP;
    
    if (faccessat(fd, "down", F_OK, 0) == 0)
        sv[svnum].state = SV_DOWN;

    if (faccessat(fd, "once", F_OK, 0) == 0)
        sv[svnum].state = SV_ONCE;
}

/* creates a fifo ctl/@name/fifo and ctl/@name/status */
void create_files(char *name)
{
    int ctlfd, ctlsvfd;

    mkdirat_f(AT_FDCWD, "ctl", S_IRWXU);
    ctlfd = open("ctl", O_DIRECTORY | O_CLOEXEC);

    if (has_log)
        logfd = open("log", O_DIRECTORY | O_CLOEXEC);
 
    mkdirat_f(ctlfd, name, S_IRWXU);

    ctlsvfd = openat(ctlfd, name, O_DIRECTORY);

    mkfifoat_f(ctlsvfd, "fifo", S_IRWXU);
    fifofd = openat(ctlsvfd, "fifo", O_RDWR | O_CLOEXEC | O_NONBLOCK);

    statusfd = openat(ctlsvfd, "status", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    close(ctlsvfd);
    close(ctlfd);
}

void do_exit()
{
    struct timespec ts = { .tv_sec = 0, .tv_nsec = 1000 };

    sv[0].state = SV_DOWN;

    if (sv[0].pid != 0) {
        kill(sv[0].pid, SIGTERM);
        nanosleep(&ts, NULL);
        kill(sv[0].pid, SIGCONT);
        nanosleep(&ts, NULL);
        kill(sv[0].pid, SIGKILL);
    }

    sig_handle(SIGCHLD, SIG_DFL);

    if (has_log && sv[1].pid != 0) {
        close(pipefd[0]);

        /* wait a while for the log process
         * to stop, if it doesn't stop, kill it
         */
        for (int i = 0; i < 5; i++) {
            nanosleep(&ts, NULL);
            pid_t logpid = waitpid(sv[1].pid, NULL, WNOHANG);

            if (logpid > 0)
                goto done;
        }

        kill(sv[1].pid, SIGTERM);
        nanosleep(&ts, NULL);
        kill(sv[1].pid, SIGKILL);
    }

done:
    exit(0);
}

void handle_fifo()
{
    int buf;

    /* lowercase letter = command for main service
     * uppercase letter = command for log service
     * (except for 'x' and 's')
     */
    while(read(fifofd, &buf, 1) != -1) {
        int svnum = 0;

        switch (buf) {
        /* up */
        case 'U':
            svnum = 1;
        case 'u':
            sv[svnum].state = SV_UP;

            if (sv[svnum].pid == 0)
                run_service();
            break;
        /* down */
        case 'D':
            svnum = 1;
        case 'd':
            sv[svnum].state = SV_DOWN;

            if (sv[svnum].pid != 0) {
                kill(sv[svnum].pid, SIGTERM);
                kill(sv[svnum].pid, SIGCONT);
            }
            break;
        /* once */
        case 'O':
            svnum = 1;
        case 'o':
            sv[svnum].state = SV_ONCE;

            if (sv[svnum].pid == 0)
                run_service();
            break;
        /* pause */
        case 'P':
            svnum = 1;
        case 'p':
            kill(sv[svnum].pid, SIGSTOP);
            break;
        /* cont */
        case 'C':
            svnum = 1;
        case 'c':
            kill(sv[svnum].pid, SIGCONT);
            break;
        /* term */
        case 'T':
            svnum = 1;
        case 't':
            kill(sv[svnum].pid, SIGTERM);
            break;
        /* status */
        case 's':
            lseek(statusfd, 0, SEEK_SET);
            write(statusfd, sv, sizeof(struct svdir));

            if (has_log)
                write(statusfd, sv + 1, sizeof(struct svdir));
            break;
        /* exit */
        case 'x':
            do_exit();
        }
    }
}

void handle_sigchld()
{
    pid_t zombie_pid;
    int wstatus;

    while ((zombie_pid = waitpid(-1, &wstatus, WNOHANG)) > 0) {
        int svnum;

        if (sv[0].pid == zombie_pid)
            svnum = 0;
        else if (sv[1].pid == zombie_pid)
            svnum = 1;
        else
            continue;

        if (svnum == 0)
            check_files(AT_FDCWD, 0);
        else
            check_files(logfd, 1);

        if (sv[svnum].files & HAS_FINISH) {
            char exit_status[5] = {0}, signal[5] = {0};

            if (WIFEXITED(wstatus)) {
                snprintf(exit_status, 4, "%d", WEXITSTATUS(wstatus));

                signal[0] = '0';
            } else {
                exit_status[0] = '-';
                exit_status[1] = '1';
            }

            if (WIFSIGNALED(wstatus))
                snprintf(signal, 4, "%d", WTERMSIG(wstatus));

            pid_t pid = fork();
 
            if (pid == 0)
                execl("./finish", "./finish", exit_status, signal, NULL);
        }

        if (sv[svnum].state == SV_ONCE && WIFEXITED(wstatus)) {
            sv[svnum].state = SV_DOWN;
            sv[svnum].pid = 0;

            return;
        }

        if (sv[svnum].state == SV_UP) {
            if (svnum == 0)
                run_service();
            else
                run_log_service();

            return;
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        fprintf(stderr, "usage: %s dir\n", argv[0]);
        return 1;
    }

    dir = argv[1];

    if (chdir(dir) == -1)
        err(1, "%s", argv[1]);

    has_log = is_dir("log");

    sig_handle(SIGCHLD, handle_sigchld);
    sig_handle(SIGTERM, do_exit);

    create_files(argv[1]);

    check_files(AT_FDCWD, 0);

    if (has_log)
        check_files(logfd, 1);

    struct pollfd pfd;

    pfd.fd = fifofd;
    pfd.events = POLLIN;

    if (has_log) {
        if (pipe2(pipefd, O_CLOEXEC) == -1) {
            warn("%s", "pipe");
            has_log = 0;
        }
    }

    if (sv[0].state != SV_DOWN)
        run_service();

    if (has_log && sv[0].state != SV_DOWN)
        run_log_service();

    while (1) {
        poll(&pfd, 1, -1);

        if (pfd.revents & POLLIN)
            handle_fifo();
    }
}

void run_service()
{
    if (access("run", X_OK) == -1)
        err(1, "can't execute run");

    pid_t pid = fork();

    if (pid == 0) {
        if (sv[0].files & HAS_PGRP)
            setsid();

        if (has_log) {
            close(1);
            dup(pipefd[1]);
        }

        execl("./run", "./run", dir, NULL);
    }
    
    sv[0].pid = pid;
    sv[0].time_started = time(NULL);
}

void run_log_service()
{
    if (access("log/run", X_OK) == -1)
        err(1, "can't execute log/run");

    pid_t logpid = fork();

    if (logpid == 0) {
        if (sv[1].files & HAS_PGRP)
            setsid();

        close(0);
        dup(pipefd[0]);

        chdir("log");

        execl("./run", "./run", NULL);
    }

    sv[1].pid = logpid;
    sv[1].time_started = time(NULL);
}
