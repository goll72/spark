#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <err.h>

#include <sys/wait.h>
#include <sys/reboot.h>

#include "sig_handle.h"
#include "init.h"

char *stages[3] = {
    STAGE_DIR"/init",
    STAGE_DIR"/services",
    STAGE_DIR"/halt"
};

pid_t curr_pid = 0;
int curr_stage = 1;
int halt_type = POWEROFF;

void run_stage(int);
void handle_sigchld();

void halt()
{
    kill(-1, SIGTERM);
    kill(-1, SIGKILL);

    switch (halt_type) {
    case KEXEC:
        reboot(RB_KEXEC);
        break;
    case POWEROFF:
        reboot(RB_POWER_OFF);
        break;
    case REBOOT:
        reboot(RB_AUTOBOOT);
        break;
    case HALT:
        reboot(RB_HALT_SYSTEM);
        break;
    }
    
    /* should never be reached */
    exit(1);
}

void prepare_halt(int signal)
{
    if (signal == SIGRTMIN+KEXEC) {
        int fd = open("/sys/kernel/kexec_loaded", O_RDONLY);
        char buf = '0';

        int ret = read(fd, &buf, 1);

        if (ret == -1 || buf == '0') {
            warnx("No kernel has been loaded\n");
            return;
        }
    }

    halt_type = signal - SIGRTMIN;

    struct timespec ts = { .tv_sec = 0, .tv_nsec = 500 };
    nanosleep(&ts, NULL);

    kill(curr_pid, SIGHUP);
}

char *get_halt_str()
{
    switch (halt_type) {
    case KEXEC:
        return "kexec";
    case POWEROFF:
        return "poweroff";
    case REBOOT:
        return "reboot";
    case HALT:
        return "halt";
    /* should never be reached */
    default:
        return "0";
    }
}

void run_stage(int stage)
{
    curr_stage = stage;

    pid_t pid = fork();

    if (pid == 0) {
        if (curr_stage == 3) {
            execl(stages[stage - 1], stages[stage - 1], get_halt_str(), NULL);
            err(1, "%s", stages[stage - 1]);
        } else {
            execl(stages[stage - 1], stages[stage - 1], NULL);
            err(1, "%s", stages[stage - 1]);
        }
    }

    curr_pid = pid; 
}

void handle_sigchld()
{
    pid_t zombie_pid;
    int wstatus;

    while ((zombie_pid = waitpid(-1, &wstatus, WNOHANG)) > 0) {
        if (zombie_pid == curr_pid) {
            switch (curr_stage) {
            case 1:
                if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus) == 100)
                    run_stage(3);
                else
                    run_stage(2);
                break;
                
            case 2:
                if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus) == 111)
                    run_stage(2);
                else
                    run_stage(3);
                break;
                
            case 3:
                sig_handle(SIGCHLD, SIG_IGN);
                halt();
                break;
            }
        }
    }
}

int main()
{
    if (getpid() != 1) {
        puts("spark version 0.1 - must be run as init");
        return 1;
    }

    sig_handle(SIGCHLD, handle_sigchld);

    sig_handle(SIGRTMIN+KEXEC, prepare_halt);
    sig_handle(SIGRTMIN+POWEROFF, prepare_halt);
    sig_handle(SIGRTMIN+REBOOT, prepare_halt);
    sig_handle(SIGRTMIN+HALT, prepare_halt);

    run_stage(1);
 
    while (1)
        pause();
}
