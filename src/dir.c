#include "dir.h"

#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>

int mkdirat_f(int fd, char *dir, mode_t mode)
{
    int status = mkdirat(fd, dir, mode);

    if (status == -1 && errno == EEXIST) {
        if (is_dir(dir)) {
            return fchmodat(fd, dir, mode, 0);
        } else {
            unlinkat(fd, dir, 0);
            return mkdirat(fd, dir, mode);
        }
    }

    return status;
}

int is_dir(char *name) 
{
    struct stat st;
    stat(name, &st);

    return S_ISDIR(st.st_mode);
}
