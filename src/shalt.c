#include <stdio.h>
#include <err.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <utmp.h>
#include <libgen.h>

#include <sys/utsname.h>
#include <sys/time.h>
#include <sys/reboot.h>

#include "init.h"

#ifndef OUR_WTMP
#define OUR_WTMP "/var/log/wtmp"
#endif

#ifndef OUR_UTMP
#define OUR_UTMP "/run/utmp"
#endif

void write_wtmp(int boot);

int main(int argc, char *argv[]) 
{
    char do_sync = 1, do_force = 0, do_wtmp = 1;
    int opt;

    int action = 0;

    char *name = basename(argv[0]);

    if (strcmp(name, "shalt") == 0)
        action = HALT;
    else if (strcmp(name, "sreboot") == 0)
        action = REBOOT;
    else if (strcmp(name, "spoweroff") == 0)
        action = POWEROFF;
    else
        warnx("no default behavior, needs to be called as shalt/sreboot/spoweroff");

    while ((opt = getopt(argc, argv, "nwdfk")) != -1)
        switch (opt) {
        case 'n':
            do_sync = 0;
            break;
        case 'w':
            write_wtmp(0);
            return 0;
        case 'd':
            do_wtmp = 0;
            break;
        case 'f':
            do_force = 1;
            break;
        case 'k':
            action = KEXEC;
            break;
        default:
            fprintf(stderr, "usage: %s [OPTIONS]\n", name);
            fprintf(stderr, "    -n  do not sync\n"
                            "    -w  only write a wtmp record and exit\n"
                            "    -d  do not write a wtmp record\n"
                            "    -f  force immediate action\n"
                            "    -k  kexec\n");
	    return 1;
	}

    if (do_wtmp) write_wtmp(0);
    if (do_sync) sync();

    if (action == KEXEC) {
        char buf;
        
        int fd = open("/sys/kernel/kexec_loaded", O_RDONLY);
        int ret = read(fd, &buf, 1);
        close(fd);

        if (ret == -1 || buf == '0') {
            fprintf(stderr, "No kernel has been loaded!\n");
            return 1;
        }
    }

    int ret = 0;

    switch (action) {
    case HALT:
        if (do_force)
	    reboot(RB_HALT_SYSTEM);
        else
            ret = kill(1, SIGRTMIN+HALT);
	    
	    break;
    case POWEROFF:
	if (do_force)
            reboot(RB_POWER_OFF);
	else
            ret = kill(1, SIGRTMIN+POWEROFF);

	break;
    case REBOOT:
	if (do_force)
            reboot(RB_AUTOBOOT);
	else
            ret = kill(1, SIGRTMIN+REBOOT);

	break;
    case KEXEC:
        if (do_force)
            reboot(RB_KEXEC);
        else
            ret = kill(1, SIGRTMIN+KEXEC);
            
        break;
    }

    if (ret == -1)
        err(1, "failed to perform action");

    return 0;
}


void write_wtmp(int boot) 
{
    int fd;

    if ((fd = open(OUR_WTMP, O_WRONLY | O_APPEND)) == -1)
        return;

    struct utmp utmp = {0};
    struct utsname uname_buf;
    struct timeval tv;

    gettimeofday(&tv, 0);

    utmp.ut_tv.tv_sec = tv.tv_sec;
    utmp.ut_tv.tv_usec = tv.tv_usec;

    utmp.ut_type = boot ? BOOT_TIME : RUN_LVL;

    strncpy(utmp.ut_name, boot ? "reboot" : "shutdown" , sizeof utmp.ut_name);
    strncpy(utmp.ut_id  , "~~", sizeof utmp.ut_id);
    strncpy(utmp.ut_line, boot ? "~" : "~~", sizeof utmp.ut_line);

    if (uname(&uname_buf) == 0)
        strncpy(utmp.ut_host, uname_buf.release, sizeof utmp.ut_host);

    if (write(fd, &utmp, sizeof utmp) < 0)
        warn("failed to write wtmp record");

    close(fd);

    if (boot) {
        if ((fd = open(OUR_UTMP, O_WRONLY | O_APPEND)) == -1) return;
	
	if (write(fd, &utmp, sizeof utmp) == -1)
            warn("failed to write utmp record");

	close(fd);
    }
}
