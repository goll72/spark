#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <err.h>

#include "sig_handle.h"
#include "dir.h"

int have_timestamp = 0;

char *timestamp_fmt = NULL, *processor = NULL;
char *logdir;

char buf[2048], timebuf[40];

FILE *out;

void flush()
{
    fflush(out);
}

void quit()
{
    exit(0);
}

void get_timestamp()
{
    struct timespec ts;
    clock_gettime(CLOCK_TAI, &ts);
    
    /* human-readable format */
    if (timestamp_fmt) {
        struct tm *t = gmtime(&ts.tv_sec);
        strftime(timebuf, 32, timestamp_fmt, t);
    /* TAI64N */
    } else {
        return;
    }

}

void do_logging(FILE *in, FILE *out)
{
    int at_start_line = 1;

    while (1) {
        char *check = fgets(buf, 2048, in);

        if (!check) {
            switch (errno) {
            case EAGAIN:
                return;
            case EBADF:
                quit();
            default:
                err(1, "failed to read input stream");
            }
        }

        int len = strlen(buf);
        
        if (at_start_line && have_timestamp) {
            get_timestamp();
            fputs(timebuf, out);
            fputs(" ", out);
        }

        fputs(buf, out);

        if (buf[len - 1] != '\n' && at_start_line)
            at_start_line = 0;
    }
}

int main(int argc, char **argv)
{
    int opt;

    while ((opt = getopt(argc, argv, "tHsf:p:")) != -1)
        switch (opt) {
        case 't':
            have_timestamp = 1;
            break;
        case 'H':
            have_timestamp = 1;
            timestamp_fmt = "%Y-%m-%dT%H:%M:%S";
            break;
        case 'f':
            have_timestamp = 1;
            timestamp_fmt = optarg;
            break;
        case 'p':
            processor = optarg;
            break;
        default:
            fprintf(stderr, "usage: %s [-H] [-t] [-f format] [-p processor] dir\n", argv[0]);
            return 1;
        }

    /* no directory name was passed */
    if (optind - 1 == argc || argc < 2) {
        fprintf(stderr, "usage: %s [-H] [-t] [-f format] [-p processor] dir\n", argv[0]);
        return 1;
    }

    logdir = argv[optind];

    mkdirat_f(AT_FDCWD, logdir, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    if (chdir(logdir) == -1)
        err(1, "%s", argv[optind]);

    /* make sure stdin is non-blocking */
    fcntl(0, F_SETFL, O_NONBLOCK);

    int currfd = open("current", O_APPEND | O_CREAT | O_WRONLY, 
            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    
    if (currfd == -1)
        err(1, "failed to open current");

    sig_handle(SIGUSR1, flush);

    sig_handle(SIGINT, quit);
    sig_handle(SIGTERM, quit);

    out = fdopen(currfd, "a");
   
    struct pollfd pfd[1] = { 
        [0].fd = 0,
        [0].events = POLLIN
    };
    
    while (1) {
        poll(pfd, 1, -1);

        if (pfd[0].revents & POLLIN)
            do_logging(stdin, out);
    }
}
