#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <dirent.h>
#include <time.h>
#include <err.h>
#include <errno.h>

#include <sys/inotify.h>
#include <sys/wait.h>

#include "sig_handle.h"
#include "dir.h"

struct service {
    struct service *next;
    pid_t pid;
    char name[NAME_MAX + 1];
} *sv, *head;

void restart_service(int);
void kill_services(int);

int create_service(char *dir) {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("ssv", "ssv", dir, NULL);
        err(1, "exec");
    }

    /* linked list wasn't allocated yet */
    if (!sv) {
        sv = malloc(sizeof(struct service));
        memset(sv, 0, sizeof(struct service) - NAME_MAX);
        head = sv;
        
        sv->pid = pid;
        strncpy(sv->name, dir, NAME_MAX);
        
        return 0;
    }

    sv->next = malloc(sizeof(struct service));
    memset(sv->next, 0, sizeof(struct service) - NAME_MAX);
    
    sv->next->pid = pid;
    strncpy(sv->next->name, dir, NAME_MAX);
    sv = sv->next;

    return 0;
}

int delete_service(char *dir) {
    struct service *ptr = head, *prev = head;

    while (ptr) {
        if (strncmp(dir, ptr->name, NAME_MAX) == 0) {
            pid_t pid = ptr->pid;

            if (prev == head) {
                head = prev->next;
                free(prev);
            } else {
                prev->next = ptr->next;
                free(ptr);
            }

            sig_handle(SIGCHLD, SIG_IGN);
            
            kill(pid, SIGTERM);
            wait(NULL);
            
            sig_handle(SIGCHLD, restart_service);
            
            return 0;
        }
        
        prev = ptr;
        ptr = ptr->next;
    }

    return 1;
}

void dir_loop(void) {
    DIR *sv_dir = opendir(".");
    struct dirent *subdir;

    while ((subdir = readdir(sv_dir)) != NULL) {
        switch (subdir->d_type) {
        case DT_DIR:
            break;
            
        case DT_UNKNOWN: case DT_LNK:
            /* fallback to stat() as some
             * fs's do not support d_type
             */
            if(!is_dir(subdir->d_name))
                continue;
            break;

        default:
            continue;
        }

        if (subdir->d_name[0] != '.')
            create_service(subdir->d_name);
    }

    closedir(sv_dir);
}


int main(int argc, char **argv)
{
    if (argc != 2) {
        fprintf(stderr, "usage: %s service-dir\n", argv[0]);
        return 1;
    }

    /* so that we can safely kill our
     * process group without worrying
     * about killing foreign processes
     */
    setsid();

    if (chdir(argv[1]) == -1)
        err(1, "%s", argv[1]);

    sig_handle(SIGCHLD, restart_service);
    sig_handle(SIGHUP, kill_services);

    dir_loop();

    int infd = inotify_init1(IN_CLOEXEC);
    inotify_add_watch(infd, ".", IN_ONLYDIR | IN_CREATE | IN_DELETE);

    char inbuf[sizeof(struct inotify_event) + NAME_MAX + 1] 
        __attribute__ ((aligned(__alignof__(struct inotify_event))));

    while (1) {
        int ret = read(infd, inbuf, sizeof(struct inotify_event) + NAME_MAX + 1);
        
        if (ret == -1 && errno != EINTR)
            err(1, "inotify");

        struct inotify_event *ev = (struct inotify_event *) inbuf;
        
        if (!(ev->mask & IN_ISDIR) || ev->name[0] == '.')
            continue;

        if (ev->mask & IN_CREATE)
            create_service(ev->name);
        else if (ev->mask & IN_DELETE)
            delete_service(ev->name);
    }
}

void restart_service(int signal) {
    pid_t zombie_pid;
   
    while ((zombie_pid = waitpid(-1, NULL, WNOHANG)) > 0) {
        struct service *ptr = head;

        while (ptr) {
            if (ptr->pid == zombie_pid) {
                pid_t new_child_pid = fork();

                if (new_child_pid == 0) {
                    execlp("ssv", "ssv", ptr->name, NULL);
                    err(1, "exec");
                }

                ptr->pid = new_child_pid;
                break;
            }

            ptr = ptr->next;
        }
    }
}

void kill_services(int signal) {
    struct service *ptr = head;

    struct timespec ts = { .tv_sec = 0, .tv_nsec = 1000 };

    while (ptr) {
        kill(ptr->pid, SIGTERM);
        waitpid(-1, NULL, WNOHANG);

        ptr = ptr->next;
    }

    kill(0, SIGTERM);
    nanosleep(&ts, NULL);
    kill(0, SIGKILL);

    exit(88);
}
