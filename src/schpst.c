#define _DEFAULT_SOURCE

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <limits.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <err.h>

#include <sys/types.h>

#define ID_T_STRLEN ((CHAR_BIT * sizeof(id_t) - 1) / 3 + 2)

#define SET_UG (1 << 0)
#define UG_ENV (1 << 1)
#define CHROOT (1 << 2)
#define CLOSE0 (1 << 3)
#define CLOSE1 (1 << 4)
#define CLOSE2 (1 << 5)
#define S_NICE (1 << 6)
#define S_PGRP (1 << 7)

void setuidgid(int env, char *str)
{
    id_t uid = 0, gid = 0;
    char valid_uid = 0, valid_gid = 0;

    /* skip the first character since it might be ':' */
    char *gidstr = strchr(str + 1, ':');

    /* turn "x:y" into "x\0y", effectively 
     * splitting the string in two
     */
    if (gidstr) {
        *gidstr = 0;
        gidstr++;
    }

    /* treat values as uid (+ gid) */
    if (str[0] == ':') {
        uid = strtoumax(str + 1, NULL, 10);
        
        /* strtoumax() returns 0 if nothing was
         * found to convert, so we need to check that
         * the string is not "0" to make sure it 
         * is an actual error
         */
        if (errno == ERANGE || (uid == 0 && strcmp(str + 1, "0") != 0))
            errx(111, "invalid uid");
        else
            valid_uid = 1;

        if (gidstr) {
            gid = strtoumax(gidstr, NULL, 10);

            if (errno == ERANGE ||
                    (gid == 0 && strcmp(gidstr, "0") != 0))
                errx(111, "invalid gid");
            else
                valid_gid = 1;
        }
    /* treat values as username (+ group name) */
    } else {
        struct passwd *pw = getpwnam(str);
        
        if (!pw) {
            errx(111, "failed to get user information");
        } else {
            uid = pw->pw_uid;
            valid_uid = 1;
        }

        if (gidstr) {
            struct group *gr = getgrnam(gidstr);

            if (!gr) {
                errx(111, "failed to get group information");
            } else {
                gid = gr->gr_gid;
                valid_gid = 1;
            }
        }
    }
    
    /* only change the environment variables UID and GID */
    if (env) {
        /* quite ironic how the value is stored as text,
         * converted to an id_t, then converted to text again
         *
         * no, i won't write an /etc/passwd parser >:(
         */
        char buf[ID_T_STRLEN + 1];

        if (valid_uid) {
            snprintf(buf, ID_T_STRLEN, "%u", uid);
            setenv("UID", buf, 1);
        }

        if (valid_gid) {
            snprintf(buf, ID_T_STRLEN, "%u", gid);
            setenv("GID", buf, 1);
        }
    /* change the current user / group */
    } else {
        int ret;

        if (valid_uid) {
            ret = setuid(uid);

            if (ret == -1)
                err(111, "failed to set user");
        }

        if (valid_gid) {
            ret = setgid(gid);

            if (ret == -1)
                err(111, "failed to set group");
        }
    }
}

void set_nice(char *str)
{
    int nice_val = strtoimax(str, NULL, 10);

    if (errno == ERANGE ||
            (nice_val == 0 && strcmp(str + 1, "0") != 0))
        errx(111, "invalid nice value");

    if (nice(nice_val) == -1)
        err(1, "failed to set nice");
}

void close_err(int fd)
{
    int ret = close(fd);

    if (ret == -1)
        err(111, "failed to close fd %d", fd);
}

int main(int argc, char **argv)
{
    int opt, action = 0;
    char *uidgidstr, *chrootstr, *nicestr;

    while ((opt = getopt(argc, argv, "u:U:/:n:o:P012")) != -1)
        switch (opt) {
        case 'u':
            action |= SET_UG;
            action &= ~UG_ENV;
                
            uidgidstr = optarg;
            break;
        case 'U':
            action |= SET_UG;
            action |= UG_ENV;
                
            uidgidstr = optarg;
            break;
        case '/':
            action |= CHROOT;

            chrootstr = optarg;
            break;
        case 'n':
            action |= S_NICE;

            nicestr = optarg;
            break;
        case '0':
            action |= CLOSE0;
            break;
        case '1':
            action |= CLOSE1;
            break;
        case '2':
            action |= CLOSE2;
            break;
        case 'P':
            action |= S_PGRP;
            break;
        default:
            fprintf(stderr, "usage: %s [OPTIONS] prog args [...]\n", argv[0]);
            return 1;
        }
 
    /* no program was passed to execute */
    if (optind - 1 == argc) {
        fprintf(stderr, "usage: %s [OPTIONS] prog args [...]\n", argv[0]);
        return 1;
    }

    if (action & SET_UG)
        setuidgid(action & UG_ENV, uidgidstr);

    if (action & CHROOT) {
        int ret = chroot(chrootstr);
        if (ret == -1)
            err(111, "failed to chroot");
    }
    
    if (action & S_NICE)
        set_nice(nicestr);

    if (action & CLOSE0)
        close_err(0); 
    if (action & CLOSE1)
        close_err(1);
    if (action & CLOSE2)
        close_err(2);

    if (action & S_PGRP)
        setsid();

    execvp(argv[optind], &argv[optind]);

    err(88, "failed to exec");
}
