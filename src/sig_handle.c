#include "sig_handle.h"

#include <signal.h>

void sig_handle(int signal, void (*handler)(int))
{   
    struct sigaction sa;
    sa.sa_handler = handler;

    sigaction(signal, &sa, NULL);
}
