#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <err.h>
#include <libgen.h>
#include <time.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "service.h"

#define warnret(x, y, ...) do { warn(y, __VA_ARGS__); return x; } while (0)

char *sv_dir, log_sv = 0;

/* if @dir starts with a '/' it is taken as an absolute path
 * 
 * if SVDIR is set and @dir does not start with a '/' 
 * it will be taken as a path relative to SVDIR
 *
 * if SVDIR is not set, @dir will be taken as a path relative to '.'
 */
int chdir_sv(char *dir)
{
    if (dir[0] == '/') {
        if (chdir(dir) == -1)
            warnret(0, "%s", dir);

        return 1;
    }

    if (sv_dir) {
        if (chdir(sv_dir) == -1)
            warnret(0, "%s", sv_dir);

        if (chdir(dir) == -1)
            warnret(0, "%s", sv_dir);

        return 1;
    }

    if (chdir(dir) == -1)
        warnret(0, "%s", dir);

    return 1;
}

void print_status(struct svdir *sv, char *dir)
{
    if (sv->state == SV_DOWN)
        printf("down: ");
    else
        printf("run: ");

    printf("%s (pid %d) ", dir, sv->pid);

    time_t now = time(NULL);
    time_t has_run = now - sv->time_started;
    int h, m, s;

    h = has_run / (60 * 60);
    m = has_run / 60 % 60;
    s = has_run % 60;

    if (h)
        printf("%dh ", h);

    if (m)
        printf("%dm ", m);

    if (s)
        printf("%ds ", s);

    printf("(%lds)", has_run);

    putchar('\n');
}

void write_warn(int fd, void *buf, size_t count)
{
    int ret = write(fd, buf, count);

    if (ret == -1)
        warn("failed to write to fifo");
}

void do_check(int optional)
{
    if (access("check", X_OK) == 0) {
        pid_t pid = fork();

        if (pid == 0)
            execl("./check", "./check", NULL);

        int wstatus;
        waitpid(pid, &wstatus, 0);

        if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus) == 0)
            printf("ok: ");
        else
            printf("fail: ");
    } else if (!optional) {
        warnx("failed to access check file");
    }
}

void do_cmd(int cmd, char *dir)
{
    int ctlfd = open("ctl", O_DIRECTORY);
    int ctlsvfd = openat(ctlfd, basename(dir), O_DIRECTORY);
    int fifofd = openat(ctlsvfd, "fifo", O_WRONLY | O_NONBLOCK);

    if (fifofd == -1) {
        warnx("fatal: ssv is not running");
        return;
    }

    int statusfd;

    struct timespec ts = { .tv_sec = 0, .tv_nsec = 100 }; 
    char buf[sizeof(struct svdir)];
    struct svdir *sv;

    switch (cmd) {
    case 'u': case 'd': case 'o': case 'p':
    case 'c': case 't': case 'x':
        if (log_sv)
            cmd = toupper(cmd);
        write_warn(fifofd, &cmd, 1);
        break;
    case 's':
        write_warn(fifofd, "s", 1);
        nanosleep(&ts, NULL);

        statusfd = openat(ctlsvfd, "status", O_RDONLY);
        read(statusfd, buf, sizeof(struct svdir));

        sv = (struct svdir *) buf;
        print_status(sv, dir);

        break;
    /* check doesn't make sense for a log service */
    case 'k':
        do_check(0);

        write_warn(fifofd, "s", 1);
        nanosleep(&ts, NULL);

        statusfd = openat(ctlsvfd, "status", O_RDONLY);
        read(statusfd, buf, sizeof(struct svdir));

        sv = (struct svdir *) buf;
        print_status(sv, dir);
 
        break;
    case 'r':
        write_warn(fifofd, "d", 1);
        nanosleep(&ts, NULL);
        write_warn(fifofd, "u", 1);

        do_check(1);

        write_warn(fifofd, "s", 1);
        nanosleep(&ts, NULL);

        statusfd = openat(ctlsvfd, "status", O_RDONLY);

        if (read(statusfd, buf, sizeof(struct svdir)) == -1) {
            warn("failed to read status file");
            return;
        }

        sv = (struct svdir *) buf;
        print_status(sv, dir);
    }

    close(fifofd);
    close(ctlsvfd);
    close(ctlfd);
}

int main(int argc, char **argv)
{
    if (argc < 3) {
        fprintf(stderr, "usage: %s [-l] cmd service [...]\n", argv[0]);
        return 1;
    }

    int cmd, opt;

    while ((opt = getopt(argc, argv, "l")) != -1)
        switch (opt) {
        case 'l':
            log_sv = 1;
        }

    if (strcmp("up", argv[1]) == 0) {
        cmd = 'u';
    } else if (strcmp("down", argv[1]) == 0) {
        cmd = 'd';
    } else if (strcmp("once", argv[1]) == 0) {
        cmd = 'o';
    } else if (strcmp("cont", argv[1]) == 0) {
        cmd = 'c';
    } else if (strcmp("term", argv[1]) == 0) {
        cmd = 't';
    } else if (strcmp("exit", argv[1]) == 0) {
        cmd = 'x';
    } else if (strcmp("status", argv[1]) == 0) {
        cmd = 's';
    } else if (strcmp("check", argv[1]) == 0) {
        cmd = 'k';
    } else if (strcmp("restart", argv[1]) == 0) {
        cmd = 'r';
    } else {
        fprintf(stderr, "usage: %s cmd service [...]\n", argv[0]);
        return 1;
    }

    sv_dir = getenv("SVDIR");
    int orig_cwd = open(".", O_DIRECTORY);

    for (int i = optind + 1; i < argc; i++) {
        if (chdir_sv(argv[i])) {
            do_cmd(cmd, argv[i]);
            fchdir(orig_cwd);
        } else {
            warn("%s", argv[i]);
        }
    }
}
