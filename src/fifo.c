#include "fifo.h"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>

int mkfifoat_f(int fd, char *fifo, mode_t mode)
{
    int status = mkfifoat(fd, fifo, mode);

    if (status == -1 && errno == EEXIST) {
        if (is_fifo(fifo)) {
            return fchmodat(fd, fifo, mode, 0);
        } else {
            unlinkat(fd, fifo, 0);
            return mkfifoat(fd, fifo, mode);
        }
    }
    
    return -1;
}

int is_fifo(char *name) 
{
    struct stat st;
    stat(name, &st);

    return S_ISFIFO(st.st_mode);
}
